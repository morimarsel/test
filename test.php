<?php
    
function no_1($n){

    $j = 1;
    $k = 1;

    for($i = 1 ; $i<= $n ; $i++ ){

        if( $k * 3  === $j * 7 ){

            echo "Z<br>";
            $j++;
            $k++;
        }else{
            if($i%3 == 0){
                echo ($j * 7)."<br>";
                $j++;
            }else{
                echo ($k * 3)."<br>";
                $k++;
            }
        }

    }

}


function no_2(){
    $string = "Berikut adalah kisah sang gajah. Sang gajah memiliki teman
    serigala bernama DoeSang. Gajah sering dibela oleh serigala ketika harimau
    mendekati gajah.";
    $string =  strtolower($string);
    
    $newstring = str_split($string);
    $kata = "";
    foreach($newstring as $row){
        $kata .= $row;

        if(strpos($kata, "sang gajah")){
            echo "sang gajah ";
            $kata = "";
        }

        if(strpos($kata, "serigala")){
            echo "serigala ";
            $kata = "";
        }

        if(strpos($kata, "harimau")){
            echo "harimau ";
            $kata = "";
        }
    }   
    
}


if(isset($_POST['submit'])){

    if(!preg_match(@"/^[0-9a-zA-Z\W]{3,20}$/", $_POST['text'])){
        $output = "<span style='color:red'>✘</span> ";
        $msg = "Panjang password harus 8 - 32 karakter";
        
    }else if(!preg_match(@"/^(?=.*[a-z])(?=.*[A-Z])(?=.*[A-Z])[0-9a-zA-Z\W]{3,20}$/", $_POST['text'])){
        
        $output = "<span style='color:red'>✘</span> ";
        $msg = "Minimal mempunyai 1 huruf besar dan 1 huruf kecil";
    }else if(!preg_match(@"/^(?=.*\d)[0-9a-zA-Z\W]{3,20}$/", $_POST['text'])){
        $output = "<span style='color:red'>✘</span> ";
        $msg = "Minimal mempunyai 1 angka";
    }else if(!preg_match(@"/^(?=.*[\W]{2,})[0-9a-zA-Z\W]{3,20}$/", $_POST['text'])){
        $output = "<span style='color:red'>✘</span> ";
        $msg = "Minimal mempunyai 2 symbol";
    }else if(!preg_match(@"/^(?!.*?\d\d\d)[0-9a-zA-Z\W]{3,20}$/", $_POST['text'])){
        $output = "<span style='color:red'>✘</span> ";
        $msg = "angka Tidak boleh 3 kali berurutan";
    }
    else{
        $output = "<span style='color:green'>✓</span> ";
        $msg = "Kata Sandi Valid";
    }
    
    
}



function no_4($array){
    function missing_number($num_list)
    {
        $new_arr = range($num_list[0],max($num_list));                                                    
        // use array_diff to find the missing elements 
        return array_diff($new_arr, $num_list);

    }

    $result = min(missing_number($array));

    echo $result;


}

function no_5($n){
    
    for($i= 0 ; $i < $n ; $i++){

        for($j = 0; $j < $n; $j++){

            if($n >= 3 && $j > 0 && $j < $n -1){
            
                echo "O";
                
            }else{
                echo "X";
            }

        }

        echo "<br>";
        

    }
}   
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test</title>
</head>
<body>

    No.1<br>
    <?php

        no_1(13);

    ?>

    <br>
    <br>

    No.2<br>
    <?php

        no_2();

    ?>
    

    <br>
    <br>

    No.3<br>
    <form method="POST" action="">
        <input type="text" name="text" autofocus/>
        <input type="SUBMIT" name="submit"/>
        <?php if(isset($output)){echo $output;} ?>
    </form>

    <?php if(isset($msg)){echo $msg;} ?>

    <br>
    <br>

    No.4<br>
    Array: <?php print_r(array(5,2,8,4,3,10)); ?>
    <br>
    Hasilnya:
    <?php

        no_4(array(5,2,8,4,3,10));

    ?>
    
    <br>
    <br>

    No.5<br>
    
    <?php
        no_5(5);
    ?>
    
    <br>
    <br>
</body>
</html>